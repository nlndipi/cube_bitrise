package com.cube.nlnd.bitrise.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.activities.ui.theme.ComposeTestTheme
import com.cube.nlnd.bitrise.components.FullButton
import com.cube.nlnd.bitrise.ui.theme.PrimaryColor
import com.cube.nlnd.bitrise.utils.Globals

class Login : ComponentActivity()
{
	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)

		val key = Globals.getToken(this)
		if (key.isNotEmpty())
		{
			startActivity(Intent(this, MainActivity::class.java))
			finish()
		}

		setContent {
			ComposeTestTheme {
				Surface(color = MaterialTheme.colors.background) {
					FullScreen()
				}
			}
		}
	}

	@Composable
	fun FullScreen()
	{
		val (key, setKey) = remember { mutableStateOf("") }

		Column(
			modifier = Modifier
				.fillMaxWidth()
				.fillMaxHeight(),
			horizontalAlignment = Alignment.CenterHorizontally) {
			Row(
				modifier = Modifier
					.weight(.5f)
					.background(PrimaryColor)
					.fillMaxWidth(),
				verticalAlignment = Alignment.CenterVertically,
				horizontalArrangement = Arrangement.Center) {
				Image(painter = painterResource(id = R.drawable.ic_bitrise_banner), contentDescription = "")
				Image(
					painter = painterResource(id = R.drawable.cube_icon), contentDescription = "",
					modifier = Modifier
						.width(60.dp)
						.padding(20.dp, 0.dp, 0.dp, 0.dp)
				)
			}

			Column(
				modifier = Modifier
					.weight(1f)
					.fillMaxWidth()
					.padding(24.dp, 40.dp),
				horizontalAlignment = Alignment.CenterHorizontally,
				verticalArrangement = Arrangement.Top) {
				Text(
					text = stringResource(R.string.login_edittext_title), modifier = Modifier.padding(0.dp, 20.dp),
					textAlign = TextAlign.Center)
				TextField(
					value = key, onValueChange = { setKey(it) }, modifier = Modifier
						.padding(0.dp, 20.dp, 0.dp, 60.dp)
						.fillMaxWidth(),
					colors = TextFieldDefaults.textFieldColors(
						backgroundColor = Color(0x22acacac),
					),
					singleLine = true)

				FullButton(title = stringResource(R.string.login_button)) {
					if (key.isNotEmpty())
					{
						Globals.login(this@Login, key)
						startActivity(Intent(this@Login, MainActivity::class.java))
						finish()
					}
				}
			}
		}
	}
}
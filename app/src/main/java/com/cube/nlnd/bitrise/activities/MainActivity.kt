package com.cube.nlnd.bitrise.activities

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navDeepLink
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.components.Actionbar
import com.cube.nlnd.bitrise.pages.Artifacts
import com.cube.nlnd.bitrise.pages.BuildDetails
import com.cube.nlnd.bitrise.pages.HomeTab
import com.cube.nlnd.bitrise.pages.ProjectsTab
import com.cube.nlnd.bitrise.ui.theme.ComposeTestTheme
import com.cube.nlnd.bitrise.utils.UserManager
import com.cube.nlnd.bitrise.utils.toDeepLink
import com.cube.nlnd.bitrise.viewmodels.HomeTabViewModel
import com.cube.nlnd.bitrise.viewmodels.ProjectsTabViewModel
import java.io.File

class MainActivity : ComponentActivity()
{
	lateinit var homeTabViewModel: HomeTabViewModel
	lateinit var projectsTabViewModel: ProjectsTabViewModel

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)

		setupApp()

		setContent {
			ComposeTestTheme { // A surface container using the 'background' color from the theme
				FullView()
			}
		}
	}

	private fun createNotificationChannel()
	{
		val channel = NotificationChannel("download",
			"download", NotificationManager.IMPORTANCE_LOW)
		val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
		service.createNotificationChannel(channel)

		val buildChannel = NotificationChannel("build", "build", NotificationManager.IMPORTANCE_HIGH)
		buildChannel.setShowBadge(true)
		buildChannel.setAllowBubbles(true)
		service.createNotificationChannel(buildChannel)
	}

	fun logout()
	{
		getSharedPreferences("key", Activity.MODE_PRIVATE).edit().remove("key").apply()
		startActivity(Intent(this, Login::class.java))
		finish()
	}

	private fun setupApp()
	{
		createNotificationChannel()

		homeTabViewModel = HomeTabViewModel(this)
		projectsTabViewModel = ProjectsTabViewModel(this)

		val path = File(filesDir, "builds")
		if (!path.exists())
		{
			path.mkdirs()
		}
		else if (!path.isDirectory)
		{
			path.delete()
			path.mkdir()
		}
		UserManager.getUser(this)
	}

	@Composable
	private fun FullView()
	{
		val navController = rememberNavController()
		val (showBackButton, setShowBackButton) = remember { mutableStateOf(false) }

		Scaffold(topBar = { Actionbar(backButton = showBackButton, title = stringResource(R.string.app_name)) { onBackPressed() } }, bottomBar = {
			BottomNavigation {
				val navBackStackEntry by navController.currentBackStackEntryAsState()
				val currentDestination = navBackStackEntry?.destination
				tabItems.forEach { screen ->
					BottomNavigationItem(
						selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
						onClick = {
							navController.navigate(screen.route) {
								popUpTo(navController.graph.findStartDestination().id)
								launchSingleTop = true
								restoreState = true
							}
						},
						icon = { Icon(painterResource(id = screen.iconId), contentDescription = "") },
						alwaysShowLabel = true,
						label = { Text(screen.route) })
				}
			}
		}, content = {
			NavHost(navController = navController, startDestination = "Home") {
				composable("Home") {
					setShowBackButton(false)
					HomeTab(homeTabViewModel) { build ->
						navController.navigate("BuildDetails/?buildslug=${build.slug}&appslug=${build.repository.slug}")
					}
				}
				composable("Projects") {
					setShowBackButton(false)
					ProjectsTab(projectsTabViewModel)
				}
				composable("Artifacts",
					deepLinks = listOf(navDeepLink { uriPattern = "Artifacts".toDeepLink() })) {
					setShowBackButton(false)
					Artifacts()
				}
				composable("BuildDetails/?buildslug={build}&appslug={app}",
					arguments = listOf(navArgument("build") {
						type = NavType.StringType
						defaultValue = ""
					}, navArgument("app") {
						type = NavType.StringType
						defaultValue = ""
					}),
					deepLinks = listOf(navDeepLink { uriPattern = "BuildDetails/?buildslug={build}&appslug={app}".toDeepLink() })
				)
				{ backStackEntry ->
					setShowBackButton(true)
					val buildSlug = backStackEntry.arguments?.getString("build")
					val appSlug = backStackEntry.arguments?.getString("app")
					BuildDetails(buildSlug ?: "", appSlug ?: "")
				}
			}
		})
	}

	@Preview(showBackground = true)
	@Composable
	fun DefaultPreview()
	{
		ComposeTestTheme {
			FullView()
		}
	}
}

data class TabItem(val route: String, val iconId: Int)

val tabItems = listOf(
	TabItem("Home", R.drawable.ic_home_tab),
	TabItem("Projects", R.drawable.ic_projects_tab),
	TabItem("Artifacts", R.drawable.ic_artifacts_tab)
)
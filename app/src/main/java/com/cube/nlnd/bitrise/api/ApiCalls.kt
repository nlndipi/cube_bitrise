package com.cube.nlnd.bitrise.api

import com.cube.nlnd.bitrise.models.AppModel
import com.cube.nlnd.bitrise.models.ArtifactModel
import com.cube.nlnd.bitrise.models.BuildModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.awaitResponse

object ApiCalls
{
	fun getApps(author: String, onSuccess: (apps: List<AppModel>) -> Unit, onFailure: () -> Unit)
	{
		val call = RetrofitBase.service.getApps(author)
		call.enqueue(object : Callback<JsonObject>
		{
			override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>)
			{
				if (response.isSuccessful)
				{
					val data = JSONObject(response.body().toString()).getJSONArray("data")
					val result = Gson().fromJson(data.toString(), Array<AppModel>::class.java).toMutableList()
					onSuccess(result)
				}
				else
				{
					onFailure()
				}
			}

			override fun onFailure(call: Call<JsonObject>, t: Throwable)
			{
				onFailure()
			}
		})
	}

	fun getBuild(author: String, appSlug: String, buildSlug: String, onSuccess: (build: BuildModel) -> Unit)
	{
		val call = RetrofitBase.service.getBuild(author, appSlug, buildSlug)
		call.enqueue(object : Callback<JsonObject>
		{
			override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>)
			{
				if (response.isSuccessful)
				{
					val data = JSONObject(response.body().toString()).getJSONObject("data")
					val result = Gson().fromJson(data.toString(), BuildModel::class.java)
					onSuccess(result)
				}
			}

			override fun onFailure(call: Call<JsonObject>, t: Throwable)
			{
				t.printStackTrace()
			}
		})
	}

	fun getBuilds(author: String, onSuccess: (apps: List<BuildModel>) -> Unit, onFailure: () -> Unit)
	{
		val call = RetrofitBase.service.getBuilds(author)
		call.enqueue(object : Callback<JsonObject>
		{
			override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>)
			{
				if (response.isSuccessful)
				{
					val data = JSONObject(response.body().toString()).getJSONArray("data")
					val result = Gson().fromJson(data.toString(), Array<BuildModel>::class.java).toMutableList()
					onSuccess(result)
				}
				else
				{
					onFailure()
				}
			}

			override fun onFailure(call: Call<JsonObject>, t: Throwable)
			{
				t.printStackTrace()
				onFailure()
			}
		})
	}

	suspend fun getBuildsOfApp(author: String, app: AppModel): MutableList<BuildModel>
	{
		val call = RetrofitBase.service.getBuildsOfApp(author, appSlug = app.slug)
		val response = call.awaitResponse()
		val data = JSONObject(response.body().toString()).getJSONArray("data")
		val result = Gson().fromJson(data.toString(), Array<BuildModel>::class.java).toMutableList()
		result.forEach { it.repository = app }
		return result
	}

	fun getArtifacts(author: String, appSlug: String, buildSlug: String, onSuccess: (artifacts: List<ArtifactModel>) -> Unit)
	{
		val call = RetrofitBase.service.getArtifacts(author, appSlug, buildSlug)
		call.enqueue(object : Callback<JsonObject>
		{
			override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>)
			{
				val data = JSONObject(response.body().toString()).getJSONArray("data")
				val result = Gson().fromJson(data.toString(), Array<ArtifactModel>::class.java).toMutableList()
				result.forEach {
					it.appSlug = appSlug
					it.buildSlug = buildSlug
				}
				onSuccess(result)
			}

			override fun onFailure(call: Call<JsonObject>, t: Throwable)
			{

			}
		})
	}

	fun getArtifactDownloadLink(author: String, artifact: ArtifactModel, onSuccess: (link: String) -> Unit)
	{
		val call = RetrofitBase.service.getArtifactDetails(author, appSlug = artifact.appSlug!!, buildSlug = artifact.buildSlug!!, slug = artifact.slug)
		call.enqueue(object : Callback<JsonObject>
		{
			override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>)
			{
				val data = JSONObject(response.body().toString()).getJSONObject("data")
				val url = data.getString("expiring_download_url")
				onSuccess(url)
			}

			override fun onFailure(call: Call<JsonObject>, t: Throwable)
			{

			}
		})
	}
}
package com.cube.nlnd.bitrise.api

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface BitriseService
{
	@GET("v0.1/apps?limit=50&sort_by=last_build_at")
	fun getApps(@Header("Authorization") author: String): Call<JsonObject>

	@GET("v0.1/builds?status=1")
	fun getBuilds(@Header("Authorization") author: String): Call<JsonObject>

	@GET("v0.1/apps/{app-slug}/builds/{build-slug}")
	fun getBuild(@Header("Authorization") author: String, @Path("app-slug") appSlug: String, @Path("build-slug") buildSlug: String): Call<JsonObject>

	@GET("v0.1/apps/{app-slug}/builds?limit=10&trigger_event_type=push&status=1")
	fun getBuildsOfApp(@Header("Authorization") author: String, @Path("app-slug") appSlug: String): Call<JsonObject>

	@GET("v0.1/apps/{app-slug}/builds/{build-slug}/artifacts")
	fun getArtifacts(
		@Header("Authorization") author: String,
		@Path("app-slug") appSlug: String,
		@Path("build-slug") buildSlug: String): Call<JsonObject>

	@GET("v0.1/apps/{app-slug}/builds/{build-slug}/artifacts/{artifact-slug}")
	fun getArtifactDetails(
		@Header("Authorization") author: String,
		@Path("app-slug") appSlug: String,
		@Path("build-slug") buildSlug: String,
		@Path("artifact-slug") slug: String): Call<JsonObject>
}

object RetrofitBase
{
	private val retrofitInstance = Retrofit.Builder().baseUrl("https://api.bitrise.io/").addConverterFactory(GsonConverterFactory.create()).build()
	val service: BitriseService = retrofitInstance.create(BitriseService::class.java)
}
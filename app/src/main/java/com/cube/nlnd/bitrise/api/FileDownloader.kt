package com.cube.nlnd.bitrise.api

import kotlinx.coroutines.Job

data class DownloadJob(var jobId: Int, var job: Job?, var slug: String)

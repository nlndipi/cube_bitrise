package com.cube.nlnd.bitrise.components

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import com.cube.nlnd.bitrise.ui.theme.Colors

@Composable
fun Actionbar(backButton: Boolean, title: String, backbuttonAction: () -> Unit)
{
	if (backButton)
	{
		TopAppBar(title = { Text(text = title) },
			backgroundColor = MaterialTheme.colors.primary,
			contentColor = Colors.WHITE,
			elevation = 2.dp,
			navigationIcon = { IconButton(onClick = backbuttonAction) { Icon(Icons.Filled.ArrowBack, "") } })
	}
	else
	{
		TopAppBar(title = { Text(text = title) }, backgroundColor = MaterialTheme.colors.primary, contentColor = Colors.WHITE, elevation = 2.dp)
	}
}
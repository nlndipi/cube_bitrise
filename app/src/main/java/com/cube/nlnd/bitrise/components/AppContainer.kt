package com.cube.nlnd.bitrise.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.models.AppModel
import com.cube.nlnd.bitrise.ui.theme.PrimaryColor

@Composable
fun AppContainer(app: AppModel, selected: Boolean, action: (favorite: Boolean) -> Unit)
{
	val (favorite, setFavorite) = remember { mutableStateOf(selected) }
	Surface(color = Color.White, modifier = Modifier
		.padding(0.dp, 16.dp)
		.clickable(onClick = {
			action(!favorite)
			setFavorite(!favorite)
		})
		.fillMaxWidth()
		.shadow(12.dp, clip = false)) {
		Row(verticalAlignment = Alignment.Top, horizontalArrangement = Arrangement.Start, modifier = Modifier.padding(16.dp, 12.dp)) {
			Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceBetween) {
				Image(
					painter = rememberImagePainter(app.avatar_url ?: PLACEHOLDER),
					contentDescription = "App logo",
					Modifier
						.width(50.dp)
						.height(50.dp))
			}
			Column(horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.SpaceBetween) {
				ResizableText(text = app.title)
				Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End, verticalAlignment = Alignment.Bottom) {
					if (favorite)
					{
						Icon(painterResource(id = R.drawable.ic_filled_star), contentDescription = "Star", tint = PrimaryColor)
					}
					else
					{
						Icon(painterResource(id = R.drawable.ic_empty_star), contentDescription = "Star", tint = PrimaryColor)
					}
				}
			}
		}
	}
}
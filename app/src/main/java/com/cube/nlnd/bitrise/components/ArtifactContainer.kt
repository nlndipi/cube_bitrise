package com.cube.nlnd.bitrise.components

import android.app.AlertDialog
import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.services.BackgroundService
import com.cube.nlnd.bitrise.activities.MainActivity
import com.cube.nlnd.bitrise.api.ApiCalls
import com.cube.nlnd.bitrise.models.ArtifactModel
import com.cube.nlnd.bitrise.utils.Globals
import com.cube.nlnd.bitrise.utils.fileExists
import com.cube.nlnd.bitrise.utils.openFile
import java.io.File

@Composable
fun ArtifactContainer(artifactModel: ArtifactModel, action: () -> Unit)
{
	val (downloading, setDownloading) = remember { mutableStateOf(BackgroundService.downloading.value?.any { it.slug == artifactModel.slug } ?: false) }
	val context = LocalContext.current as MainActivity
	val key = Globals.getToken(context)

	BackgroundService.downloading.observe(context) { list ->
		setDownloading(list.any { it.slug == artifactModel.slug })
	}

	Surface(color = Color.White, modifier = Modifier
		.padding(0.dp, 16.dp)
		.clickable {
			if (fileExists(artifactModel.slug, context))
			{
				openFile(context, File(context.filesDir, "builds/${artifactModel.slug}.apk"))
			}
			else
			{
				ApiCalls.getArtifactDownloadLink(key, artifactModel) {
					val build = context.homeTabViewModel.builds.find { it.slug == artifactModel.buildSlug } ?: return@getArtifactDownloadLink
					val intent = Intent(context, BackgroundService::class.java).apply {
						putExtra("build", build)
						putExtra("url", it)
						putExtra("slug", artifactModel.slug)
					}
					when
					{
						downloading ->
						{
							context.stopService(intent)
						}
						BackgroundService.downloading.value != null &&
								BackgroundService.downloading.value!!.size > 0 ->
						{
							AlertDialog
								.Builder(context)
								.setTitle("Can't download")
								.setMessage("Only 1 download at a time is possible")
								.setPositiveButton("OK", null)
								.show()
						}
						else ->
						{
							Toast.makeText(context, "Download started...", Toast.LENGTH_SHORT).show()
							context.startService(intent)
						}
					}
				}
				action()
			}
		}
		.fillMaxWidth()
		.shadow(12.dp, clip = false)) {
		Column(verticalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth()) {
			Row(verticalAlignment = Alignment.CenterVertically,
				horizontalArrangement = Arrangement.Start,
				modifier = Modifier.padding(16.dp, 12.dp)) {
				Column(modifier = Modifier.weight(1f), horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.SpaceBetween) {
					Text(text = artifactModel.title, fontSize = 14.sp, fontWeight = FontWeight.Bold, modifier = Modifier.padding(12.dp, 0.dp))
					Text(text = "${(artifactModel.file_size_bytes / 1000)} KB", fontSize = 12.sp, modifier = Modifier.padding(12.dp, 0.dp))
				}

				when
				{
					fileExists(artifactModel.slug, context) ->
					{
						Icon(painterResource(id = R.drawable.ic_check), contentDescription = "Open build", tint = MaterialTheme.colors.primary)
					}
					downloading ->
					{
						Icon(painterResource(id = R.drawable.ic_cancel), contentDescription = "Cancel Download", tint = MaterialTheme.colors.primary)
					}
					else ->
					{
						Icon(painterResource(id = R.drawable.ic_download), contentDescription = "Download build", tint = MaterialTheme.colors.primary)
					}
				}
			}
		}
	}
}
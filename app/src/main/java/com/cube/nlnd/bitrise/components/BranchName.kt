package com.cube.nlnd.bitrise.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cube.nlnd.bitrise.ui.theme.Colors

@Composable
fun BranchName(name: String)
{
	Row(Modifier
		.padding(12.dp, 8.dp)
		.background(Colors.BRANCH_BACK)
		.padding(4.dp, 2.dp)) { Text(text = name, fontSize = 14.sp) }
}
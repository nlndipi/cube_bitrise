package com.cube.nlnd.bitrise.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.cube.nlnd.bitrise.models.BuildModel
import com.cube.nlnd.bitrise.utils.formatDate

const val PLACEHOLDER = "https://www.bitrise.io/assets/icons/apple-icon-180x180.png"

@Composable
fun BuildContainer(build: BuildModel, action: () -> Unit)
{
	Surface(color = Color.White,
		modifier = Modifier
			.fillMaxWidth()
			.padding(0.dp, 12.dp)
			.shadow(12.dp, clip = false)
			.clickable(onClick = action)) {
		Row(verticalAlignment = Alignment.Top, horizontalArrangement = Arrangement.Start, modifier = Modifier
			.padding(16.dp, 12.dp)) {
			Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceBetween) {
				Image(painter = rememberImagePainter(build.repository.avatar_url ?: PLACEHOLDER),
					contentDescription = "App logo",
					Modifier
						.width(50.dp)
						.height(50.dp))
				Text(text = "#${build.build_number}", fontSize = 12.sp, modifier = Modifier.padding(0.dp, 8.dp))
			}
			Column(horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.SpaceBetween) {
				ResizableText(text = build.repository.title)
				BranchName(name = build.branch)
				Text(text = formatDate(build.triggered_at), fontSize = 12.sp, modifier = Modifier.padding(12.dp, 0.dp))
			}
		}
	}
}
package com.cube.nlnd.bitrise.components

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DownloadedArtifact(slug: String, action: () -> Unit, delete: () -> Unit)
{
	val context = LocalContext.current
	val sharedPref = context.getSharedPreferences(slug, Activity.MODE_PRIVATE)
	val buildNumber = sharedPref.getString("build_number", "")
	val avatar = sharedPref.getString("avatar_url", null)
	val title = sharedPref.getString("title", "")
	val variant = sharedPref.getString("variant", "")
	val (expanded, setExpanded) = remember { mutableStateOf(false) }

	Surface(color = Color.White, modifier = Modifier
		.padding(0.dp, 16.dp)
		.fillMaxWidth()
		.shadow(12.dp, clip = false),
		onClick = {
			setExpanded(true)
		}) {
		DropdownMenu(expanded = expanded, onDismissRequest = { setExpanded(false) }) {
			DropdownMenuItem(onClick = {
				action()
				setExpanded(false)
			}) {
				Text(text = "Install")
			}
			DropdownMenuItem(onClick = {
				delete()
				setExpanded(false)
			}) {
				Text(text = "Delete")
			}
		}
		Row(verticalAlignment = Alignment.Top, horizontalArrangement = Arrangement.Start, modifier = Modifier.padding(16.dp, 12.dp)) {
			Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceBetween) {
				Image(painter = rememberImagePainter(avatar ?: PLACEHOLDER), contentDescription = "App logo",
					Modifier
						.width(50.dp)
						.height(50.dp))
				Text(text = "#${buildNumber}", fontSize = 12.sp, modifier = Modifier.padding(0.dp, 8.dp))
			}
			Column(horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.SpaceBetween) {
				ResizableText(text = title ?: "")
				Text(text = variant ?: "", fontSize = 12.sp, modifier = Modifier.padding(12.dp, 0.dp))
			}
		}
	}
}

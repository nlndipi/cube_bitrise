package com.cube.nlnd.bitrise.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.cube.nlnd.bitrise.ui.theme.PrimaryColor

@Composable
fun FullButton(title: String, action: () -> Unit)
{
	Surface(color = Color.White, modifier = Modifier
		.padding(0.dp, 8.dp)
		.clickable(onClick = action)
		.drawColoredShadow(PrimaryColor)) {
		Row(verticalAlignment = Alignment.CenterVertically,
			horizontalArrangement = Arrangement.Center,
			modifier = Modifier
				.fillMaxWidth()
				.padding(16.dp, 12.dp)) {
			Text(text = title)
		}
	}
}

fun Modifier.drawColoredShadow(
	color: Color,
	alpha: Float = .5f,
	borderRadius: Dp = 2.dp,
	shadowRadius: Dp = 20.dp,
	offsetY: Dp = 0.dp,
	offsetX: Dp = 0.dp) = this.drawBehind {
	val transparentColor = android.graphics.Color.toArgb(color.copy(alpha = 0.0f).value.toLong())
	val shadowColor = android.graphics.Color.toArgb(color.copy(alpha = alpha).value.toLong())
	this.drawIntoCanvas {
		val paint = Paint()
		val frameworkPaint = paint.asFrameworkPaint()
		frameworkPaint.color = transparentColor
		frameworkPaint.setShadowLayer(shadowRadius.toPx(), offsetX.toPx(), offsetY.toPx(), shadowColor)
		it.drawRoundRect(0f, 0f, this.size.width, this.size.height, borderRadius.toPx(), borderRadius.toPx(), paint)
	}
}
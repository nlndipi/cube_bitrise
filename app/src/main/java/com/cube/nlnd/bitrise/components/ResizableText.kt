package com.cube.nlnd.bitrise.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.unit.dp

@Composable
fun ResizableText(text: String)
{
	val textStyleBody1 = MaterialTheme.typography.body1
	var textStyle by remember { mutableStateOf(textStyleBody1) }
	var readyToDraw by remember { mutableStateOf(false) }
	Text(text = text, style = textStyle, maxLines = 1, softWrap = false, modifier = Modifier
		.padding(12.dp, 0.dp)
		.drawWithContent {
			if (readyToDraw) drawContent()
		}, onTextLayout = { textLayoutResult ->
		if (textLayoutResult.didOverflowWidth)
		{
			textStyle = textStyle.copy(fontSize = textStyle.fontSize * 0.9)
		}
		else
		{
			readyToDraw = true
		}
	})
}
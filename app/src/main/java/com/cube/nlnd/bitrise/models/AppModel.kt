package com.cube.nlnd.bitrise.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AppModel(val slug: String, val title: String, var avatar_url: String?) : Parcelable

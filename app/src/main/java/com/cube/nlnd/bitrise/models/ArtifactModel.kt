package com.cube.nlnd.bitrise.models

data class ArtifactModel(val title: String,
	val artifact_type: String,
	val slug: String,
	val file_size_bytes: Long,
	var appSlug: String?,
	var buildSlug: String?)
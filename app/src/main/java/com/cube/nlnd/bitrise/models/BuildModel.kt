package com.cube.nlnd.bitrise.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BuildModel(val slug: String,
	val triggered_at: String,
	val status_text: String,
	val branch: String,
	val build_number: String,
	var repository: AppModel) : Parcelable
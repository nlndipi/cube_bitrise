package com.cube.nlnd.bitrise.pages

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cube.nlnd.bitrise.components.DownloadedArtifact
import com.cube.nlnd.bitrise.utils.openFile
import java.io.File

@Composable
fun Artifacts()
{
	val context = LocalContext.current
	val dir = File(context.filesDir, "builds")
	val (files, setFiles) = remember { mutableStateOf(dir.listFiles()) }

	Surface(color = MaterialTheme.colors.background) {
		Column(modifier = Modifier
			.fillMaxWidth()
			.fillMaxHeight()
			.padding(16.dp, 24.dp)) {
			Text(text = "Downloaded Artifacts", fontSize = 20.sp, fontWeight = FontWeight.Bold)

			LazyColumn(Modifier
				.fillMaxHeight()
				.padding(0.dp, 16.dp)) {
				if (!files.isNullOrEmpty())
				{
					items(files.size) {
						DownloadedArtifact(readInfo(files[it]), {
							openFile(context, files[it])
						}, {
							files[it].delete()
							val list = files.toMutableList()
							list.removeAt(it)
							setFiles(list.toTypedArray())
						})
					}
				}
			}
		}
	}
}

private fun readInfo(file: File): String
{
	val name = file.name
	return name.split(".")[0]
}
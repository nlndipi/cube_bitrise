package com.cube.nlnd.bitrise.pages

import android.app.Activity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cube.nlnd.bitrise.activities.MainActivity
import com.cube.nlnd.bitrise.api.ApiCalls
import com.cube.nlnd.bitrise.components.ArtifactContainer
import com.cube.nlnd.bitrise.models.ArtifactModel
import com.cube.nlnd.bitrise.models.BuildModel
import com.cube.nlnd.bitrise.utils.Globals

@Composable
fun BuildDetails(buildSlug: String, appSlug: String)
{
	val (artifacts, setArtifacts) = remember { mutableStateOf(listOf<ArtifactModel>()) }
	val (build, setBuild) = remember { mutableStateOf<BuildModel?>(null) }
	val (loading, setLoading) = remember { mutableStateOf(false) }
	val context = LocalContext.current as MainActivity
	val key = Globals.getToken(context)
	val app = context.projectsTabViewModel.apps.find { it.slug == appSlug }

	LaunchedEffect(key1 = Unit, block = {
		setLoading(true)
		ApiCalls.getArtifacts(key, appSlug, buildSlug) {
			setArtifacts(it.filter { art -> art.artifact_type == "android-apk" })
			setLoading(false)
		}
		ApiCalls.getBuild(key, appSlug, buildSlug) {
			setBuild(it)
		}
	})

	Scaffold {
		Column(
			modifier = Modifier
				.fillMaxHeight()
				.fillMaxWidth()
				.padding(16.dp, 24.dp)) {

			Text(text = "Artifacts for ${app?.title}", fontSize = 20.sp, fontWeight = FontWeight.Bold)
			Text(text = "Build Number #${build?.build_number}", fontSize = 20.sp, fontWeight = FontWeight.Bold)

			if (loading)
			{
				Column(
					modifier = Modifier
						.fillMaxWidth()
						.fillMaxHeight()
						.padding(16.dp, 0.dp),
					horizontalAlignment = Alignment.CenterHorizontally,
					verticalArrangement = Arrangement.Center) {
					Text(text = "Fetching all of the artifacts...")
					LinearProgressIndicator(Modifier.padding(0.dp, 12.dp))
				}
			}
			else
			{
				LazyColumn(Modifier.padding(0.dp, 16.dp)) {
					items(artifacts.size) {
						ArtifactContainer(artifactModel = artifacts[it]) {
							context.getSharedPreferences(artifacts[it].slug, Activity.MODE_PRIVATE).edit().apply {
								putString("build_number", build?.build_number)
								putString("avatar_url", app?.avatar_url)
								putString("title", app?.title)
								putString("variant", artifacts[it].title)
								apply()
							}
						}
					}
					item {
						Spacer(modifier = Modifier.height(10.dp))
					}
				}
			}
		}
	}
}


package com.cube.nlnd.bitrise.pages

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cube.nlnd.bitrise.components.AppContainer
import com.cube.nlnd.bitrise.viewmodels.ProjectsTabViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshState

@Composable
fun ProjectsTab(viewModel: ProjectsTabViewModel)
{
	val isRefreshing = SwipeRefreshState(viewModel.loading.value)

	Surface(color = MaterialTheme.colors.background) {
		SwipeRefresh(state = isRefreshing, onRefresh = { viewModel.loadData() }) {
			Column(
				modifier = Modifier
					.fillMaxWidth()
					.fillMaxHeight()
					.padding(16.dp, 24.dp)) {
				Text(text = "Apps", fontSize = 20.sp, fontWeight = FontWeight.Bold)

				if (viewModel.loading.value)
				{
					Column(
						modifier = Modifier
							.fillMaxWidth()
							.fillMaxHeight()
							.padding(16.dp, 0.dp),
						horizontalAlignment = Alignment.CenterHorizontally,
						verticalArrangement = Arrangement.Center) {
						Text(text = "Fetching all of the apps...")
						LinearProgressIndicator(Modifier.padding(0.dp, 12.dp))
					}
				}
				else
				{
					LazyColumn(Modifier.padding(0.dp, 16.dp, 0.dp, 26.dp)) {
						items(viewModel.apps.size) {
							AppContainer(app = viewModel.apps[it], viewModel.favorites.contains(viewModel.apps[it].slug)) { favorite ->
								viewModel.toggleFavorite(viewModel.apps[it].slug, favorite)
							}
						}
					}
				}
			}
		}
	}
}

package com.cube.nlnd.bitrise.services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.api.DownloadJob
import com.cube.nlnd.bitrise.models.BuildModel
import com.cube.nlnd.bitrise.utils.toDeepLink
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

private const val BUFFER_LENGTH_BYTES = 1024 * 8
private const val HTTP_TIMEOUT = 30

class BackgroundService : Service()
{
	private lateinit var notificationManager: NotificationManager
	private lateinit var notificationBuilder: Notification.Builder
	private lateinit var build: BuildModel
	lateinit var slug: String

	var serviceId = 0

	override fun onBind(intent: Intent): IBinder?
	{
		return null
	}

	override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
	{
		build = intent?.extras?.getParcelable("build")!!
		slug = intent.extras?.getString("slug")!!
		val url = intent.extras?.getString("url")
		serviceId = startId

		val pendingIntent = Intent(Intent.ACTION_VIEW, "BuildDetails/?buildslug=${build.slug}&appslug=${build.repository.slug}".toDeepLink().toUri())
			.let { notificationIntent ->
				PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)
			}

		notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
		notificationBuilder = Notification.Builder(this, "download")
			.setContentTitle("Downloading")
			.setContentText(build.repository.title)
			.setSmallIcon(R.mipmap.ic_launcher)
			.setContentIntent(pendingIntent)

		val notification = notificationBuilder.setProgress(100, 0, false).build()
		startForeground(serviceId, notification)

		if (url != null)
		{
			download(slug, url)
		}

		return super.onStartCommand(intent, flags, startId)
	}

	private fun download(slug: String, url: String)
	{
		val okHttpClient = OkHttpClient.Builder().build().newBuilder()
			.connectTimeout(HTTP_TIMEOUT.toLong(), TimeUnit.SECONDS).readTimeout(HTTP_TIMEOUT.toLong(), TimeUnit.SECONDS).build()

		val coroutine = MainScope()
		val job = coroutine.launch(Dispatchers.IO) {
			val request = Request.Builder().url(url).build()
			val response = okHttpClient.newCall(request).execute()
			val body = response.body()
			val responseCode = response.code()

			if (responseCode >= HttpURLConnection.HTTP_OK && responseCode < HttpURLConnection.HTTP_MULT_CHOICE && body != null)
			{
				val targetFile = File(cacheDir, "$slug.apk")

				val length = body.contentLength()
				body.byteStream().apply {
					targetFile.outputStream().use { fileOut ->
						var bytesCopied = 0
						val buffer = ByteArray(BUFFER_LENGTH_BYTES)
						var bytes = read(buffer)
						while (bytes >= 0)
						{
							fileOut.write(buffer, 0, bytes)
							bytesCopied += bytes
							bytes = read(buffer)
							withContext(Dispatchers.Main) {
								val progress = (((bytesCopied * 100L) / length).toInt())
								notificationBuilder.setProgress(100, progress, false)
								notificationManager.notify(serviceId, notificationBuilder.build())
							}
						}
					}
				}
				saveAndStop(targetFile, slug)
			}
			else
			{
				throw IllegalArgumentException("Error occurred when do http get $url")
			}
		}

		addItem(DownloadJob(serviceId, job, slug))
	}

	override fun onDestroy()
	{
		cancel()
		super.onDestroy()
	}

	private fun saveAndStop(fileSrc: File, slug: String)
	{
		val pendingIntent: PendingIntent = Intent(Intent.ACTION_VIEW, "Artifacts".toDeepLink().toUri()).let { notificationIntent ->
			PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)
		}
		val completeNotification = Notification.Builder(applicationContext, "download")
			.setContentTitle("Download Completed")
			.setContentText(build.repository.title)
			.setSmallIcon(R.mipmap.ic_launcher)
			.setContentIntent(pendingIntent)
			.setAutoCancel(true)
			.build()
		notificationManager.notify(serviceId + 1, completeNotification)

		FileInputStream(fileSrc).use { inp ->
			FileOutputStream(File(filesDir, "builds/$slug.apk")).use { outp ->
				inp.copyTo(outp)
			}
		}
		stopSelf()
	}

	private fun cancel()
	{
		downloading.value?.find { it.jobId == serviceId }?.let {
			it.job?.cancel()
			removeItem(it)
		}

		val targetFile = File(cacheDir, "builds/${slug}.apk")
		if (targetFile.exists())
		{
			targetFile.delete()
		}
	}

	companion object
	{
		val downloading: MutableLiveData<MutableList<DownloadJob>> = MutableLiveData(mutableListOf())

		fun addItem(item: DownloadJob)
		{
			val clone = downloading.value
			clone?.add(item)
			downloading.postValue(clone)
		}

		fun removeItem(item: DownloadJob)
		{
			val clone = downloading.value
			clone?.removeIf { it.jobId == item.jobId }
			downloading.postValue(clone)
		}
	}
}
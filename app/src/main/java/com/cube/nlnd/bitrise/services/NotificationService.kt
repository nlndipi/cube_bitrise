package com.cube.nlnd.bitrise.services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import androidx.core.net.toUri
import com.cube.nlnd.bitrise.R
import com.cube.nlnd.bitrise.utils.toDeepLink
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class NotificationService : FirebaseMessagingService()
{
	override fun onNewToken(token: String)
	{
		super.onNewToken(token)
	}

	override fun onMessageReceived(message: RemoteMessage)
	{
		val data = message.data

		val title = data["title"]

		val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

		when (title)
		{
			"New Build" ->
			{
				val pendingIntent: PendingIntent = Intent(Intent.ACTION_VIEW,
					"BuildDetails/?buildslug=${data["buildSlug"]}&appslug=${data["appSlug"]}".toDeepLink().toUri()).let { notificationIntent ->
					PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)
				}
				val completeNotification = Notification.Builder(applicationContext, "build")
					.setContentTitle(title)
					.setContentText(data["body"])
					.setSmallIcon(R.mipmap.ic_launcher)
					.setContentIntent(pendingIntent)
					.setAutoCancel(true)
					.build()

				notificationManager.notify(Random().nextInt(), completeNotification)
			}
			else ->
			{
				super.onMessageReceived(message)
			}
		}
	}
}
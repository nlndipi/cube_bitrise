package com.cube.nlnd.bitrise.ui.theme

import androidx.compose.ui.graphics.Color

val PrimaryColor = Color(0xff492f5c )
val PrimaryColorVariant = Color(0xff5a405d )
val Teal200 = Color(0xFF03DAC5)

object Colors {
	val WHITE = Color(0xffffffff)
	val BRANCH_BACK = Color(0x290fc38a)
}
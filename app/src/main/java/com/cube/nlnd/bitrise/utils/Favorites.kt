package com.cube.nlnd.bitrise.utils

import android.app.Activity
import android.content.Context

object Favorites
{
	fun addFavorite(slug: String, context: Context)
	{
		val favorites = extractFavorites(context).toMutableList()
		if (!favorites.contains(slug))
		{
			favorites.add(slug)
		}
		save(context, favorites)
	}

	fun removeFavorite(slug: String, context: Context)
	{
		val favorites = extractFavorites(context).toMutableList()
		if (favorites.contains(slug))
		{
			favorites.remove(slug)
		}
		save(context, favorites)
	}

	fun getFavorites(context: Context): List<String>
	{
		return extractFavorites(context)
	}

	private fun extractFavorites(context: Context): List<String>
	{
		val sharedPref = context.getSharedPreferences("Favorites", Activity.MODE_PRIVATE)
		val favoritesStr = sharedPref.getString("Favorites", null)
		return if (favoritesStr.isNullOrEmpty())
		{
			arrayListOf()
		}
		else
		{
			favoritesStr.split("-")
		}
	}

	private fun save(context: Context, list: List<String>)
	{
		val sharedPref = context.getSharedPreferences("Favorites", Activity.MODE_PRIVATE)
		sharedPref.edit().putString("Favorites", list.joinToString("-")).apply()

		UserManager.user.favorites = list.toMutableList()
		UserManager.saveUser(context)
	}
}
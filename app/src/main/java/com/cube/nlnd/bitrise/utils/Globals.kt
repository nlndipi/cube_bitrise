package com.cube.nlnd.bitrise.utils

import android.app.Activity
import android.content.Context

object Globals
{
	fun getToken(context: Context): String
	{
		return context.getSharedPreferences("key", Activity.MODE_PRIVATE).getString("key", "") ?: ""
	}

	fun login(context: Context, key: String)
	{
		context.getSharedPreferences("key", Activity.MODE_PRIVATE).edit().apply {
			putString("key", key)
			apply()
		}
	}
}
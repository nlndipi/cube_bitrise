package com.cube.nlnd.bitrise.utils

import android.app.Activity
import android.content.Context
import android.provider.Settings
import com.cube.nlnd.bitrise.models.AppModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson

object UserManager
{
	var user = UserModel("", mutableListOf(), "")

	fun getUser(context: Context)
	{
		val userStr = context.getSharedPreferences("user", Activity.MODE_PRIVATE).getString("user", null)
		if (userStr == null)
		{
			initializeUser(context)
		}
		else
		{
			user = Gson().fromJson(userStr, UserModel::class.java)
		}
	}

	private fun initializeUser(context: Context)
	{
		val id = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
		FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
			user = UserModel(id, mutableListOf(), task.result)
			saveUser(context)
		}
	}

	fun saveUser(context: Context)
	{
		context.getSharedPreferences("user", Activity.MODE_PRIVATE).edit().putString("user", Gson().toJson(user)).apply()
		val database = Firebase.firestore
		val data = hashMapOf("id" to user.id, "token" to user.token, "favorites" to user.favorites.joinToString(";"))
		database.collection("users").document(user.id).set(data).addOnFailureListener {
			it.printStackTrace()
		}
	}

	fun updateApps(apps: List<AppModel>)
	{
		val database = Firebase.firestore
		val batch = database.batch()
		apps.forEach {
			batch.set(database.collection("apps").document(it.slug), hashMapOf("title" to it.title))
		}
		batch.commit()
	}
}

data class UserModel(val id: String, var favorites: MutableList<String>, val token: String)
package com.cube.nlnd.bitrise.utils

import android.content.Context
import android.content.Intent
import androidx.core.content.FileProvider
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

fun formatDate(date: String): String
{
	val dateFormat1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault())
	val dateFormat2 = SimpleDateFormat("MMM dd, yyyy HH:mm", Locale.getDefault())
	return dateFormat2.format(dateFormat1.parse(date))
}

fun getDate(date: String): Date
{
	val dateFormat1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault())
	return dateFormat1.parse(date)
}

fun fileExists(slug: String, context: Context): Boolean
{
	val file = File(context.filesDir, "builds/$slug.apk")
	return file.exists()
}

fun openFile(context: Context, file: File)
{
	context.startActivity(Intent(Intent.ACTION_VIEW).apply {
		addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
		setDataAndType(
			FileProvider.getUriForFile(
				context,
				context.applicationContext.packageName + ".provider", file), "application/vnd.android.package-archive")
	})
}

fun String.toDeepLink() = "app://com.cube.nlnd.bitrise/$this"
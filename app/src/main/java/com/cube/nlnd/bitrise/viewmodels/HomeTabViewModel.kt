package com.cube.nlnd.bitrise.viewmodels

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.lifecycleScope
import com.cube.nlnd.bitrise.activities.MainActivity
import com.cube.nlnd.bitrise.api.ApiCalls
import com.cube.nlnd.bitrise.models.BuildModel
import com.cube.nlnd.bitrise.utils.Favorites
import com.cube.nlnd.bitrise.utils.Globals
import com.cube.nlnd.bitrise.utils.getDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeTabViewModel(val context: MainActivity)
{
	var builds: MutableList<BuildModel> = mutableStateListOf()
	val loading = mutableStateOf(false)
	private val key = Globals.getToken(context)

	init
	{
		loadData()
	}

	fun loadData()
	{
		loading.value = true
		val favorites = Favorites.getFavorites(context)
		if (favorites.isEmpty())
		{
			ApiCalls.getBuilds(key, {
				builds = it.toMutableList()
				loading.value = false
			}, {
				(context).logout()
			})
		}
		else
		{
			ApiCalls.getApps(key, { allApps ->
				builds = mutableStateListOf()
				context.lifecycleScope.launch(Dispatchers.Main) {
					for (slug in favorites)
					{
						val app = allApps.find { it.slug == slug }
						if (app != null)
						{
							builds.addAll(ApiCalls.getBuildsOfApp(key, app))
						}
					}
					builds.sortByDescending { getDate(it.triggered_at) }
					loading.value = false
				}
			}, {
				context.logout()
			})
		}
	}
}
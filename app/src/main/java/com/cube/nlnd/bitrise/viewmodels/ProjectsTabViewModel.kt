package com.cube.nlnd.bitrise.viewmodels

import android.content.Context
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import com.cube.nlnd.bitrise.activities.MainActivity
import com.cube.nlnd.bitrise.api.ApiCalls
import com.cube.nlnd.bitrise.models.AppModel
import com.cube.nlnd.bitrise.utils.Favorites
import com.cube.nlnd.bitrise.utils.Globals
import com.cube.nlnd.bitrise.utils.UserManager

class ProjectsTabViewModel(val context: Context)
{
	val apps: MutableList<AppModel> = mutableStateListOf()
	val loading = mutableStateOf(false)
	var favorites = Favorites.getFavorites(context)
	private val key = Globals.getToken(context)

	init
	{
		loadData()
	}

	fun loadData()
	{
		favorites = Favorites.getFavorites(context)
		loading.value = true
		ApiCalls.getApps(key, {
			apps.addAll(it)
			loading.value = false
		}, {
			(context as MainActivity).logout()
		})
	}

	fun toggleFavorite(slug: String, favorite: Boolean)
	{
		if (favorite)
		{
			Favorites.addFavorite(slug, context)
		}
		else
		{
			Favorites.removeFavorite(slug, context)
		}
	}
}